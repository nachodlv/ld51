// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "Damage/LDDamageInterface.h"

#include "GameFramework/Character.h"
#include "SheepCharacter.generated.h"

class ALD51GameMode;
class ASheepCharacter;

DECLARE_DYNAMIC_MULTICAST_DELEGATE_ThreeParams(FOnAIHealthChanged, ASheepCharacter*, Character, float, NewHealth, float, OldHealth);

UCLASS()
class LD51_API ASheepCharacter : public ACharacter, public ILDDamageInterface
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintPure)
	bool IsDead() const;

	UFUNCTION(BlueprintPure)
	bool GetIsSpider() const;

	UFUNCTION(BlueprintPure)
	float GetHealthPercentage() const;

	UFUNCTION(BlueprintPure)
	float GetTargetScore(FName ScoreTag) const;

	// Sets default values for this character's properties
	ASheepCharacter();

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	UFUNCTION(BlueprintPure)
	FVector GetShootLocation() const;

	// ~ Begin ILDDamageInterface
	UFUNCTION(BlueprintCallable)
	virtual float GetCurrentHealth() const override { return Health; }
	
	UFUNCTION(BlueprintCallable)
	virtual float GetMaxHealth() const override { return MaxHealth; }

	UFUNCTION(BlueprintCallable)
	virtual void SetHealth(float NewHealth) override;

	UFUNCTION(BlueprintCallable)
	virtual void ApplyDamage(AActor* DamageInstigator, const FHitResult& Hit, float Damage) override;
	// ~ End ILDDamageInterface

	UFUNCTION(BlueprintImplementableEvent)
	void MakeAttackAnimation();

	UFUNCTION(BlueprintImplementableEvent)
	void EatAnimation();

	UFUNCTION(BlueprintImplementableEvent)
	void ShowDamageFeedback(AActor* DamageInstigator, const FHitResult& Hit, float Damage);

	UFUNCTION(BlueprintImplementableEvent)
	void ShowPetFeedback();

	UPROPERTY(BlueprintAssignable)
	FOnAIHealthChanged OnHealthChanged;

protected:
	UFUNCTION()
	void MontageNotifyReceived(FName NotifyName, const FBranchingPointNotifyPayload& BranchingPointPayload);
	
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UFUNCTION(BlueprintPure, BlueprintImplementableEvent)
	TArray<USkeletalMeshComponent*> GetMeshes() const;

private:	
	void MoveForward(float AxisValue);

	ALD51GameMode* GetLDGameMode(UWorld* World);

	UPROPERTY(EditDefaultsOnly, Category = "LD")
	float MaxHealth = 100;

	UPROPERTY(EditDefaultsOnly, Category = "LD")
	float MaxHealthMultiplier = 1.2f;

	UPROPERTY(VisibleAnywhere, Category = "LD")
	float Health = 0.0f;

	UPROPERTY(EditDefaultsOnly, Category = "LD")
	TMap<FName, float> ScoreValues;

	UPROPERTY(EditDefaultsOnly, Category = "LD")
	FName ShootSocketName = TEXT("Head");

	UPROPERTY(EditDefaultsOnly, Category = "LD")
	FName AIKillScoreTag = TEXT("AI.Score.Kill");

	UPROPERTY(EditDefaultsOnly, Category = "LD")
	float KnockbackForce = 150.0f;

	UPROPERTY(EditDefaultsOnly, Category = "LD")
	float ImmortalityDistance = 1000.0f;
};
