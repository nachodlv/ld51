// Copyright Epic Games, Inc. All Rights Reserved.

#include "LD51GameMode.h"
#include "LD51Character.h"

#include "DayNightCyle/LDDayNightCycler.h"
#include "DayNightCyle/LDMapTransitioner.h"

#include "UObject/ConstructorHelpers.h"

ALD51GameMode::ALD51GameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPerson/Blueprints/BP_ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}

	DayNightCyclerClass = ULDDayNightCycler::StaticClass();
}

void ALD51GameMode::InitGame(const FString& MapName, const FString& Options, FString& ErrorMessage)
{
	Super::InitGame(MapName, Options, ErrorMessage);

	DayNightCycler = NewObject<ULDDayNightCycler>(this, DayNightCyclerClass);
	MapTransitioner = NewObject<ULDMapTransitioner>(this, MapTransitionerClass);
}

void ALD51GameMode::StartPlay()
{
	Super::StartPlay();

	GameplayStartTime = GetWorld()->GetTimeSeconds();
	GetDayNightCycler()->StartDay();
	GetMapTransitioner()->Initialize();

	GetDayNightCycler()->OnCycleEnded.AddDynamic(this, &ALD51GameMode::OnCycleChange);
}

void ALD51GameMode::AddGameScore(float InScoreValue)
{
	GameScore += InScoreValue;
	if (InScoreValue != 0)
	{
		if (!GetWorld())
		{
			return;
		}

		if (APlayerController* PlayerController = GetWorld()->GetFirstPlayerController())
		{
			if (ALD51Character* PlayerCharacter = PlayerController->GetPawn<ALD51Character>())
			{
				OnGameScoreUpdated.Broadcast(PlayerCharacter);
			}
		}
	}
}


void ALD51GameMode::OnCycleChange_Implementation(ULDDayNightCycler* Cycler, const FLDCycleProperties& NewCycle,
	const FLDCycleProperties& PreviousCycle)
{
	if (PreviousCycle.Cycle == ELDCycleState::Night)
	{
		AddGameScore(ScoreForNight);
		++NightsSurvived;
	}
}
