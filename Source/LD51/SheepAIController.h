// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "Apple.h"
#include "SheepCharacter.h"
#include "SheepAIController.generated.h"

UCLASS()
class LD51_API ASheepAIController : public AAIController
{
	GENERATED_BODY()
	
public:
	ASheepAIController();

	virtual void Tick(float DeltaSeconds) override;

	UFUNCTION(BlueprintCallable)
	void ActorDetected(AApple* AppleDetected);

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void SetIsSpider(bool bInSpider);
	void SetIsSpider_Implementation(bool bInSpider);

	UFUNCTION(BlueprintPure)
	bool IsSpider() const { return bIsSpider; }

	UFUNCTION(BlueprintCallable)
	void StartAttack();

	void MakeAttack();

	UPROPERTY(EditAnywhere)
	bool bIsSpider = false;

	UPROPERTY(EditAnywhere)
	float Range = 300.0f;

	UPROPERTY(EditAnywhere)
	float Damage = 10.0f;

	UPROPERTY(EditAnywhere)
	float DamageIncreaseMultiplier = 1.2f;

	UPROPERTY(EditAnywhere)
	float SphereRadius = 50.0f;

	UPROPERTY(EditAnywhere)
	float TimeToWaitWhenSpider = 2.0f;

	UPROPERTY()
	FVector AILocation;

	UPROPERTY()
	FVector PlayerLocation;

	UPROPERTY(EditAnywhere)
	float TimeBetweenAttacks = 1.0f;

	float LastAttackTime = 0.0f;

	float BecomeSpiderTime = 0.0f;

	UPROPERTY(Transient)
	TObjectPtr<AApple> LastAppleDetected = nullptr;

protected:
	virtual void BeginPlay() override;

	virtual void OnPossess(APawn* InPawn) override;

	UFUNCTION()
	void OnPawnHealthChanged(ASheepCharacter* InCharacter, float NewHealth, float OldHealth);

private:
	bool AttackTrace(TArray<FHitResult>& Hit, FVector& AttackDirection);
};
