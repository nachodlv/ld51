#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"

#include "Pickups/LDPickup.h"

#include "Apple.generated.h"

class AApple;

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnAppleUnburied, AApple*, Apple);

UCLASS()
class LD51_API AApple : public ALDPickup
{
	GENERATED_BODY()
	
public:	
	AApple();

	UFUNCTION(BlueprintPure)
	bool IsBuried() const { return bBuried; }

	UFUNCTION(BlueprintCallable)
	void SetBuried(bool bInBuried);

	virtual void Grabbed_Implementation() override;

	UPROPERTY(BlueprintAssignable)
	FOnAppleUnburied OnAppleUnburied;
	
protected:
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere)
	bool bBuried = false;
};
