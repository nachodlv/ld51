#include "Apple.h"

AApple::AApple()
{
	PrimaryActorTick.bCanEverTick = false;
}

void AApple::SetBuried(bool bInBuried)
{
	const bool bOldBuriedState = bBuried;
	bBuried = bInBuried;
	if (!bInBuried && bOldBuriedState != bInBuried)
	{
		OnAppleUnburied.Broadcast(this);
	}
}

void AApple::Grabbed_Implementation()
{
	Super::Grabbed_Implementation();
	SetBuried(false);
}

void AApple::BeginPlay()
{
	Super::BeginPlay();
	
}

