// Fill out your copyright notice in the Description page of Project Settings.


#include "SheepAIController.h"
#include "Apple.h"
#include "LD51Character.h"
#include "LD51GameMode.h"
#include "NavigationSystem.h"
#include "SheepCharacter.h"
#include "Kismet/GameplayStatics.h"

ASheepAIController::ASheepAIController()
{
	PrimaryActorTick.bCanEverTick = true;

}

void ASheepAIController::BeginPlay()
{
	Super::BeginPlay();

	APawn* PlayerPawn = UGameplayStatics::GetPlayerPawn(GetWorld(), 0);

	if (bIsSpider && PlayerPawn)
	{
		MoveToActor(PlayerPawn);
	}
}

void ASheepAIController::OnPossess(APawn* InPawn)
{
	Super::OnPossess(InPawn);
	
	Cast<ASheepCharacter>(InPawn)->OnHealthChanged.AddDynamic(this, &ASheepAIController::OnPawnHealthChanged);
}

void ASheepAIController::OnPawnHealthChanged(ASheepCharacter* InCharacter, float NewHealth, float OldHealth)
{
	if (NewHealth <= 0)
	{
		StopMovement();
		ClearFocus(EAIFocusPriority::Gameplay);
	}
}

void ASheepAIController::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	if (GetPawn<ASheepCharacter>()->GetCurrentHealth() <= 0)
	{
		return;
	}

	if(bIsSpider)
	{
		const float TimeSeconds = GetWorld()->GetTimeSeconds();
		if (TimeSeconds - BecomeSpiderTime < TimeToWaitWhenSpider)
		{
			return;
		}

		APawn* PlayerPawn = UGameplayStatics::GetPlayerPawn(GetWorld(), 0);
		if (!PlayerPawn)
		{
			return;
		}

		AILocation = GetPawn()->GetActorLocation();
		PlayerLocation = PlayerPawn->GetActorLocation();
		FVector SubstractDistances = (AILocation - PlayerLocation);
		float DistanceToPlayer = SubstractDistances.Size();
		if (DistanceToPlayer <= Range)
		{
			SetFocus(PlayerPawn);
			if ((TimeSeconds - LastAttackTime) > TimeBetweenAttacks)
			{
				StopMovement();
				StartAttack();
			}
		}
		else if (!IsFollowingAPath())
		{
			MoveToActor(PlayerPawn);
		}
	}
	else if (!IsFollowingAPath() && LastAppleDetected && !LastAppleDetected->IsGrabbed())
	{
		// UNavigationSystemV1* NavSystem = UNavigationSystemV1::GetCurrent(GetWorld());
		// const FNavAgentProperties& AgentProperties = GetNavAgentPropertiesRef();
		// FPathFindingQuery Query;
		// NavSystem->FindPathAsync(AgentProperties, );
		MoveToActor(LastAppleDetected);	
		ASheepCharacter* Sheep = Cast<ASheepCharacter>(GetCharacter());
		Sheep->EatAnimation();
	}
}

void ASheepAIController::ActorDetected(AApple* AppleDetected)
{
	if (!bIsSpider)
	{
		LastAppleDetected = AppleDetected;
	}
}

void ASheepAIController::SetIsSpider_Implementation(bool bInSpider)
{
	StopMovement();
	ClearFocus(EAIFocusPriority::Gameplay);
	LastAppleDetected = nullptr;
	bIsSpider = bInSpider;
	BecomeSpiderTime = GetWorld()->GetTimeSeconds();
}

void ASheepAIController::StartAttack()
{
	LastAttackTime = GetWorld()->GetTimeSeconds();

	ASheepCharacter* Sheep = Cast<ASheepCharacter>(GetCharacter());
	Sheep->MakeAttackAnimation();
}

void ASheepAIController::MakeAttack()
{
	TArray<FHitResult> Hit;
	FVector AttackDirection;
	const bool bSuccess = AttackTrace(Hit, AttackDirection);

	if (bSuccess)
	{
		int32 HitCount = Hit.Num();
		if (HitCount > 0)
		{
			for (const FHitResult Hits : Hit)
			{
				if (ALD51Character* PlayerCharacter = Cast<ALD51Character>(Hits.GetActor()))
				{
					float FinalDamage = Damage;
					if (ALD51GameMode* GameMode = GetWorld()->GetAuthGameMode<ALD51GameMode>())
					{
						FinalDamage +=  FinalDamage * GameMode->GetNightsSurvived() * DamageIncreaseMultiplier;
					}
					const float PlayerHealth = PlayerCharacter->GetCurrentHealth();
					const float PlayerDamaged = PlayerHealth - FinalDamage;
					PlayerCharacter->SetHealth(PlayerDamaged);
					break;
				}
			}
		}
	}
}

bool ASheepAIController::AttackTrace(TArray<FHitResult>& Hit, FVector& AttackDirection)
{
	FVector Location;
	FRotator Rotation;
	GetPlayerViewPoint(Location, Rotation);
	FVector End = Location + Rotation.Vector() * Range;
	AttackDirection = End;

	const FCollisionShape Sphere = FCollisionShape::MakeSphere(SphereRadius);
	FCollisionQueryParams Params;

	Params.AddIgnoredActor(this);
	Params.AddIgnoredActor(GetOwner());

	return GetWorld()->SweepMultiByChannel(Hit, Location, End, FQuat::Identity, ECollisionChannel::ECC_Pawn, Sphere, Params);
}
