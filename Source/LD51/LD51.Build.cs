// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class LD51 : ModuleRules
{
	public LD51(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(new string[]
		{
			"Core", 
			"CoreUObject", 
			"Engine", 
			"InputCore", 
			"HeadMountedDisplay"
		});
	}
}
