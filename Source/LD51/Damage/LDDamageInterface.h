﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "LDDamageInterface.generated.h"

UINTERFACE(MinimalAPI, BlueprintType, NotBlueprintable)
class ULDDamageInterface : public UInterface
{
	GENERATED_BODY()
};

class LD51_API ILDDamageInterface
{
	GENERATED_BODY()

public:

	UFUNCTION(BlueprintCallable)
	virtual float GetCurrentHealth() const { return 0.0f; }
	
	UFUNCTION(BlueprintCallable)
	virtual float GetMaxHealth() const { return 0.0f; }

	UFUNCTION(BlueprintCallable)
	virtual void SetHealth(float NewHealth) {}

	UFUNCTION(BlueprintCallable)
	virtual void ApplyDamage(AActor* DamageInstigator, const FHitResult& Hit, float Damage) {}
};
