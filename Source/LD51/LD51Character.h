// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Damage/LDDamageInterface.h"
#include "GameFramework/Character.h"
#include "LD51Character.generated.h"

class ALDPickup;
struct FLDCycleProperties;
class ALD51Character;
class ALD51GameMode;
class ASheepCharacter;

DECLARE_DYNAMIC_MULTICAST_DELEGATE_ThreeParams(FOnHealthChange, ALD51Character*, Character, float, NewHealth, float, OldHealth);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnPerformAnimActionEnded, ALD51Character*, Character);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnPickupChanged, ALD51Character*, Character, ALDPickup*, NewPickup);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FLDActionEventDelegate);


UCLASS(config=Game)
class ALD51Character : public ACharacter, public ILDDamageInterface
{
	GENERATED_BODY()

	/** Camera boom positioning the camera behind the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

	/** Follow camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* FollowCamera;
public:
	ALD51Character();

	//-----------PUBLIC DEBUG-------------
	UFUNCTION(BlueprintImplementableEvent, Category = "LD51|Debug")
	void DebugDrawLocation(FVector Location, float Thickness, float Time, FLinearColor DrawColor = FColor::Cyan);
	UFUNCTION(BlueprintImplementableEvent, Category = "LD51|Debug")
	void DebugDrawTrace(FVector LocationStart, FVector LocationEnd, float Thickness, float Time, FLinearColor DrawColor = FColor::Cyan);
	//---------END PUBLIC DEBUG-----------

	// ~ Begin ILDDamageInterface
	UFUNCTION(BlueprintCallable)
	virtual float GetCurrentHealth() const override { return CurrentHealth; }
	
	UFUNCTION(BlueprintCallable)
	virtual float GetMaxHealth() const override { return MaxHealth; }
	
	UFUNCTION(BlueprintCallable)
	virtual void SetHealth(float NewHealth) override;

	UFUNCTION(BlueprintCallable)
	virtual void ApplyDamage(AActor* DamageInstigator, const FHitResult& Hit, float Damage) override;
	// ~ End ILDDamageInterface

	UFUNCTION(BlueprintCallable)
	bool SetPerformingAnimAction(bool NewValue);

	UFUNCTION(BlueprintPure)
	ALDPickup* GetCurrentPickup() const { return CurrentPickup; }

	UFUNCTION(BlueprintCallable)
	void StartPickupDropAction();

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void AddVelocityPowerup(float NewSpeed, float Duration);
	
	/** Base turn rate, in deg/sec. Other scaling may affect final turn rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Input)
	float TurnRateGamepad;

	UPROPERTY(BlueprintAssignable)
	FOnHealthChange OnHealthChange;

	UPROPERTY(BlueprintAssignable)
	FOnPerformAnimActionEnded OnPerformAnimActionEnded;

	UPROPERTY(BlueprintAssignable)
	FOnPickupChanged OnPickupChanged;

protected:
	UFUNCTION()
	void OnMontageNotifyReceived(FName NotifyName, const FBranchingPointNotifyPayload& BranchingPointPayload);
	
	virtual void BeginPlay() override;

	/** Called for click action input */
	void ClickActionEvent();

	/** Action Events Triggers */
	void StartPetAction();
	void StartFiregunAction();
	void StartUsePickupAction();
	/** Blueprint Events */
	UFUNCTION(BlueprintImplementableEvent)
	void PetActionEvent(ASheepCharacter* Target);
	UFUNCTION(BlueprintImplementableEvent)
	void FiregunActionEvent(ASheepCharacter* Target);
	
	/** Manages a single timed event */
	void StartTimedAction(float Time, TFunction<void(ASheepCharacter*)> ActionEventDelegate, ASheepCharacter* Target);
	UFUNCTION()
	void EndTimedAction();

	/** Called for forwards/backward input */
	void MoveForward(float Value);

	/** Called for side to side input */
	void MoveRight(float Value);

	/** 
	 * Called via input to turn at a given rate. 
	 * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	 */
	void TurnAtRate(float Rate);

	/**
	 * Called via input to turn look up/down at a given rate. 
	 * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	 */
	void LookUpAtRate(float Rate);

	/** Handler for when a touch input begins. */
	void TouchStarted(ETouchIndex::Type FingerIndex, FVector Location);

	/** Handler for when a touch input stops. */
	void TouchStopped(ETouchIndex::Type FingerIndex, FVector Location);

	// APawn interface
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	// End of APawn interface

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void OnVelocityPowerupEnded();

	UFUNCTION(BlueprintPure)
	bool CanMove() { return GetCurrentHealth() > 0; }

public:
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
	/** Returns FollowCamera subobject **/
	FORCEINLINE class UCameraComponent* GetFollowCamera() const { return FollowCamera; }

private:
	/** Pet ability properties */
	UPROPERTY(EditAnywhere, Category = "LD51|Abilities|Pet")
	FName PetAbilityTag;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "LD51|Abilities|Pet", meta = (AllowPrivateAccess = "true"))
	FVector PetAbilityOffset;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "LD51|Abilities|Pet", meta = (AllowPrivateAccess = "true"))
	float PetTraceRadius;
	UPROPERTY(EditAnywhere, Category = "LD51|Abilities|Pet")
	float PetActionTime = 1.0f;

	/** Shoot ability properties */
	UPROPERTY(EditAnywhere, Category = "LD51|Abilities|Shoot")
	FName ShootAbilityTag;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "LD51|Abilities|Shoot", meta = (AllowPrivateAccess = "true"))
	FVector ShootAbilityGunLocation;
	UPROPERTY(EditAnywhere, Category = "LD51|Abilities|Shoot")
	float ShootTraceRadius;
	UPROPERTY(EditAnywhere, Category = "LD51|Abilities|Shoot", BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	float ShootDistance;
	UPROPERTY(EditAnywhere, Category = "LD51|Abilities|Shoot")
	float ShootDamage = 10.0f;
	UPROPERTY(EditAnywhere, Category = "LD51|Abilities|Pet")
	float ShootActionTime = 0.5f;

	/** Pickup ability properties */
	UPROPERTY(EditAnywhere, Category = "LD51|Abilities|Pickup", BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	float PickupRadius = 50.0f;
	UPROPERTY(Transient)
	TObjectPtr<ALDPickup> CurrentPickup = nullptr;

	UPROPERTY(BlueprintReadOnly, Category = "LD51|Abilities|Shoot", meta = (AllowPrivateAccess = "true"))
	bool bPerformingAnimAction;
	FTimerHandle AnimTimerHandle;

	/** Debug properties */
	UPROPERTY(EditAnywhere, Category = "LD51|Debug")
	bool bShowDebugTraces = false;
	UPROPERTY(EditAnywhere, Category = "LD51|Debug")
	bool bHideShootTrace = false;
	UPROPERTY(EditAnywhere, Category = "LD51|Debug")
	bool bHidePickupDebugTraces = false;
	UPROPERTY(EditAnywhere, Category = "LD51|Debug")
	FLinearColor TraceSuccessColor = FLinearColor::Green;
	UPROPERTY(EditAnywhere, Category = "LD51|Debug")
	FLinearColor TraceFailColor = FLinearColor::Red;

	/** Character helpers */
	TArray<FHitResult> MakeCustomTrace(FVector TraceLocationStart, FVector TraceLocationEnd, float Radius, FName TraceTag, bool bIsSingle = false);
	FLDCycleProperties GetCurrentWorldCycle(UWorld* World);
	ALD51GameMode* GetLDGameMode(UWorld* World);

	UPROPERTY(EditAnywhere, Category = "LD51")
	float MaxHealth = 100.0f;
	
	float CurrentHealth = 0.0f;

	FTimerHandle VelocityPowerupHandle;
};

