﻿#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "LDPickup.generated.h"

class ALD51Character;
UCLASS()
class LD51_API ALDPickup : public AActor
{
	GENERATED_BODY()

public:
	ALDPickup();

	UFUNCTION(BlueprintNativeEvent)
	void Grabbed();
	virtual void Grabbed_Implementation();

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void Dropped();
	void Dropped_Implementation();

	UFUNCTION(BlueprintNativeEvent)
	bool CanBeGrabbed() const;
	virtual bool CanBeGrabbed_Implementation() const { return true; }

	UFUNCTION(BlueprintPure)
	bool IsGrabbed() const { return bIsGrabbed; }

	UFUNCTION(BlueprintCallable)
	void SetGrabbed(bool bInGrabbed) { bIsGrabbed = bInGrabbed; }

	UFUNCTION(BlueprintNativeEvent)
	void UsePickup(ALD51Character* Character);
	virtual void UsePickup_Implementation(ALD51Character* Character);

	UFUNCTION(BlueprintPure, BlueprintNativeEvent)
	UTexture2D* GetPickupIcon() const;
	UTexture2D* GetPickupIcon_Implementation() const { return nullptr; }

private:
	bool bIsGrabbed = false;
};
