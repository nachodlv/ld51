﻿#include "LDPickup.h"


ALDPickup::ALDPickup()
{
	PrimaryActorTick.bCanEverTick = false;
}

void ALDPickup::Grabbed_Implementation()
{
	bIsGrabbed = true;
}

void ALDPickup::Dropped_Implementation()
{
	bIsGrabbed = false;
}

void ALDPickup::UsePickup_Implementation(ALD51Character* Character)
{
}

