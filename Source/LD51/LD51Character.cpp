// Copyright Epic Games, Inc. All Rights Reserved.

#include "LD51Character.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/InputComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/Controller.h"
#include "GameFramework/SpringArmComponent.h"
#include "Pickups/LDPickup.h"

// Non-Default Includes
#include "GameplayTagsManager.h"
#include "LD51GameMode.h"
#include "DayNightCyle/LDDayNightCycler.h"
#include "SheepCharacter.h"

//////////////////////////////////////////////////////////////////////////
// ALD51Character

ALD51Character::ALD51Character()
{
	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// set our turn rate for input
	TurnRateGamepad = 50.f;

	// Don't rotate when the controller rotates. Let that just affect the camera.
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Character moves in the direction of input...	
	GetCharacterMovement()->RotationRate = FRotator(0.0f, 500.0f, 0.0f); // ...at this rotation rate

	// Note: For faster iteration times these variables, and many more, can be tweaked in the Character Blueprint
	// instead of recompiling to adjust them
	GetCharacterMovement()->JumpZVelocity = 700.f;
	GetCharacterMovement()->AirControl = 0.35f;
	GetCharacterMovement()->MaxWalkSpeed = 500.f;
	GetCharacterMovement()->MinAnalogWalkSpeed = 20.f;
	GetCharacterMovement()->BrakingDecelerationWalking = 2000.f;

	// Create a camera boom (pulls in towards the player if there is a collision)
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->TargetArmLength = 400.0f; // The camera follows at this distance behind the character	
	CameraBoom->bUsePawnControlRotation = true; // Rotate the arm based on the controller

	// Create a follow camera
	FollowCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("FollowCamera"));
	FollowCamera->SetupAttachment(CameraBoom, USpringArmComponent::SocketName); // Attach the camera to the end of the boom and let the boom adjust to match the controller orientation
	FollowCamera->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	// Note: The skeletal mesh and anim blueprint references on the Mesh component (inherited from Character) 
	// are set in the derived blueprint asset named ThirdPersonCharacter (to avoid direct content references in C++)
}

//////////////////////////////////////////////////////////////////////////
// Input

void ALD51Character::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	// Set up gameplay key bindings
	check(PlayerInputComponent);
	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ACharacter::Jump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &ACharacter::StopJumping);

	/* PlayerInputComponent->BindAxis("Move Forward / Backward", this, &ALD51Character::MoveForward);
	PlayerInputComponent->BindAxis("Move Right / Left", this, &ALD51Character::MoveRight);

	// We have 2 versions of the rotation bindings to handle different kinds of devices differently
	// "turn" handles devices that provide an absolute delta, such as a mouse.
	// "turnrate" is for devices that we choose to treat as a rate of change, such as an analog joystick
	PlayerInputComponent->BindAxis("Turn Right / Left Mouse", this, &APawn::AddControllerYawInput);
	PlayerInputComponent->BindAxis("Turn Right / Left Gamepad", this, &ALD51Character::TurnAtRate);
	PlayerInputComponent->BindAxis("Look Up / Down Mouse", this, &APawn::AddControllerPitchInput);
	PlayerInputComponent->BindAxis("Look Up / Down Gamepad", this, &ALD51Character::LookUpAtRate);

	// handle touch devices
	PlayerInputComponent->BindTouch(IE_Pressed, this, &ALD51Character::TouchStarted);
	PlayerInputComponent->BindTouch(IE_Released, this, &ALD51Character::TouchStopped); */

	PlayerInputComponent->BindAction("ClickAction", IE_Pressed, this, &ALD51Character::ClickActionEvent);

	PlayerInputComponent->BindAction("PickupDrop", IE_Pressed, this, &ALD51Character::StartPickupDropAction);
	PlayerInputComponent->BindAction("UsePickup", IE_Pressed, this, &ALD51Character::StartUsePickupAction);
}

void ALD51Character::OnVelocityPowerupEnded_Implementation()
{
	const ALD51Character* DefaultCharacter = GetDefault<ALD51Character>(GetClass());
	GetCharacterMovement()->MaxWalkSpeed = DefaultCharacter->GetCharacterMovement()->MaxWalkSpeed;
}

void ALD51Character::SetHealth(float NewHealth)
{
	const float OldHealth = CurrentHealth;
	CurrentHealth = FMath::Clamp(NewHealth, 0.0f, 100.0f);
	OnHealthChange.Broadcast(this, CurrentHealth, OldHealth);

	if (GetCurrentHealth() <= 0)
	{
		GetLDGameMode(GetWorld())->GameOver();
	}
}

void ALD51Character::ApplyDamage(AActor* DamageInstigator, const FHitResult& Hit, float Damage)
{
	SetHealth(CurrentHealth - Damage);
}

bool ALD51Character::SetPerformingAnimAction(bool NewValue)
{
	if (!NewValue && bPerformingAnimAction)
	{
		OnPerformAnimActionEnded.Broadcast(this);
	}
	return bPerformingAnimAction = NewValue;
}

void ALD51Character::OnMontageNotifyReceived(FName NotifyName,
	const FBranchingPointNotifyPayload& BranchingPointPayload)
{
	UE_LOG(LogTemp, Warning, TEXT("Notify received: %s"), *NotifyName.ToString());
}

void ALD51Character::BeginPlay()
{
	Super::BeginPlay();
	SetHealth(MaxHealth);
	UAnimInstance* AnimInstance = GetMesh()->GetAnimInstance();
	AnimInstance->OnPlayMontageNotifyBegin.AddDynamic(this, &ALD51Character::OnMontageNotifyReceived);
}

void ALD51Character::ClickActionEvent()
{
	FLDCycleProperties CurrentCycleProperties = GetCurrentWorldCycle(GetWorld());

	if (CurrentCycleProperties.Cycle == ELDCycleState::Day)
	{
		StartPetAction();
	}
	else if (CurrentCycleProperties.Cycle == ELDCycleState::Night)
	{
		StartFiregunAction();
	}
}

void ALD51Character::StartPetAction()
{
	if (bPerformingAnimAction)
	{
		return;
	}

	FVector PlayerLocation = GetActorLocation();
	FVector PetOffsetOriented = (GetActorForwardVector() * PetAbilityOffset.X) + (GetActorRightVector() * PetAbilityOffset.Y);
	FVector TraceLocation = PlayerLocation + PetOffsetOriented;

	TArray<FHitResult> Hits = MakeCustomTrace(TraceLocation, TraceLocation, PetTraceRadius, PetAbilityTag);

	int32 HitCount = Hits.Num();

	if (SetPerformingAnimAction(HitCount > 0))
	{
		ASheepCharacter* FirstSheepCharacter = Cast<ASheepCharacter>(Hits[0].GetActor());
		FirstSheepCharacter->ShowPetFeedback();
		//TODO: Be able (awake) to send the whole array of characters
		TWeakObjectPtr<ALD51Character> This (this);
		StartTimedAction(PetActionTime, [This](ASheepCharacter* Target)
		{
			if (This.IsValid())
			{
				This->PetActionEvent(Target);
			}
		}, FirstSheepCharacter);
		for (const FHitResult Hit : Hits)
		{
			if (ASheepCharacter* SheepCharacter = Cast<ASheepCharacter>(Hit.GetActor()))
			{
				if (ALD51GameMode* GameMode = GetLDGameMode(GetWorld()))
				{
					GameMode->AddGameScore(SheepCharacter->GetTargetScore(PetAbilityTag));
				}
			}
		}
	}

	if (bShowDebugTraces)
	{
		FLinearColor CurrentTraceColor = HitCount > 0 ? TraceSuccessColor : TraceFailColor;
		DebugDrawLocation(TraceLocation, PetTraceRadius, 0.5f, CurrentTraceColor);
	}
}

void ALD51Character::StartFiregunAction()
{
	FVector PlayerLocation = GetActorLocation();
	FVector ShootOffsetOriented = (GetActorForwardVector() * ShootAbilityGunLocation.X) +
		(GetActorRightVector() * ShootAbilityGunLocation.Y) + (FVector::UpVector * ShootAbilityGunLocation.Z);
	FVector TraceStart = PlayerLocation + ShootOffsetOriented;
	FVector TraceEnd = TraceStart + (GetActorForwardVector() * ShootDistance);

	TArray<FHitResult> Hits = MakeCustomTrace(TraceStart, TraceEnd, ShootTraceRadius, ShootAbilityTag, true);

	int32 HitCount = Hits.Num();

	ASheepCharacter* SheepCharacter = nullptr;
	if (Hits.Num() > 0)
	{
		SheepCharacter = Cast<ASheepCharacter>(Hits[0].GetActor());
		if (SheepCharacter)
		{
			SheepCharacter->ApplyDamage(this, Hits[0], ShootDamage);
		}
	}
	SetPerformingAnimAction(true);
	TWeakObjectPtr<ALD51Character> This (this);
	StartTimedAction(ShootActionTime, [This](ASheepCharacter* Target)
	{
		if (This.IsValid())
		{
			This->FiregunActionEvent(Target);
		} 
	}, SheepCharacter);

	if (bShowDebugTraces && !bHideShootTrace)
	{
		FLinearColor CurrentTraceColor = HitCount > 0 ? TraceSuccessColor : TraceFailColor;
		DebugDrawTrace(TraceStart, TraceEnd, ShootTraceRadius, 0.25f, CurrentTraceColor);
	}
}

void ALD51Character::StartTimedAction(float Time, TFunction<void(ASheepCharacter*)> ActionEventDelegate, ASheepCharacter* Target)
{
	ActionEventDelegate(Target);
	GetWorldTimerManager().SetTimer(AnimTimerHandle, this, &ALD51Character::EndTimedAction, Time);
}

void ALD51Character::EndTimedAction()
{
	GetWorldTimerManager().ClearTimer(AnimTimerHandle);
	SetPerformingAnimAction(false);
}

void ALD51Character::StartUsePickupAction()
{
	if (CurrentPickup)
	{
		CurrentPickup->UsePickup(this);
		CurrentPickup = nullptr;
		OnPickupChanged.Broadcast(this, nullptr);
	}
}

void ALD51Character::StartPickupDropAction()
{
	if (CurrentPickup)
	{
		CurrentPickup->SetActorLocation(GetActorLocation(), false, nullptr, ETeleportType::TeleportPhysics);
		CurrentPickup->Dropped();
		CurrentPickup = nullptr;
		OnPickupChanged.Broadcast(this, nullptr);
	}
	else
	{
		// Pickup
		const FVector ActorLocation = GetActorLocation() - FVector::UpVector * 50.0f;
		TArray<FHitResult> Hits;
		GetWorld()->SweepMultiByChannel(Hits, ActorLocation, ActorLocation, FQuat::Identity,
			ECollisionChannel::ECC_Visibility, FCollisionShape::MakeSphere(PickupRadius));
		bool bGrabPickup = false;
		for (const FHitResult& Hit : Hits)
		{
			ALDPickup* Pickup = Cast<ALDPickup>(Hit.GetActor());
			if (Pickup && Pickup->CanBeGrabbed())
			{
				bGrabPickup = true;
				CurrentPickup = Pickup;
				Pickup->Grabbed();
				Pickup->SetActorLocation(FVector(5000.0f, 5000.0f, 500.0f)); // Move outside the map
				OnPickupChanged.Broadcast(this, Pickup);
				break;
			}
		}

		if (bShowDebugTraces && !bHidePickupDebugTraces)
		{
			DrawDebugSphere(GetWorld(), ActorLocation, PickupRadius, 15,
				bGrabPickup ? FColor::Green : FColor::Red, false, 4.0f);
		}
	}
}

void ALD51Character::AddVelocityPowerup_Implementation(float NewSpeed, float Duration)
{
	GetCharacterMovement()->MaxWalkSpeed = NewSpeed;
	FTimerManager& TimerManager = GetWorldTimerManager();
	TimerManager.ClearTimer(VelocityPowerupHandle);

	const FTimerDelegate Delegate = FTimerDelegate::CreateUObject(this, &ALD51Character::OnVelocityPowerupEnded);
	TimerManager.SetTimer(VelocityPowerupHandle, Delegate, Duration, false);
}

void ALD51Character::TouchStarted(ETouchIndex::Type FingerIndex, FVector Location)
{
	Jump();
}

void ALD51Character::TouchStopped(ETouchIndex::Type FingerIndex, FVector Location)
{
	StopJumping();
}

void ALD51Character::TurnAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerYawInput(Rate * TurnRateGamepad * GetWorld()->GetDeltaSeconds());
}

void ALD51Character::LookUpAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerPitchInput(Rate * TurnRateGamepad * GetWorld()->GetDeltaSeconds());
}

void ALD51Character::MoveForward(float Value)
{
	if ((Controller != nullptr) && (Value != 0.0f) && CanMove())
	{
		// find out which way is forward
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		// get forward vector
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);
		AddMovementInput(Direction, Value);
	}
}

void ALD51Character::MoveRight(float Value)
{
	if ( (Controller != nullptr) && (Value != 0.0f) && CanMove() )
	{
		// find out which way is right
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);
	
		// get right vector 
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);
		// add movement in that direction
		AddMovementInput(Direction, Value);
	}
}

//---------HELPERS----------

TArray<FHitResult> ALD51Character::MakeCustomTrace(FVector TraceLocationStart, FVector TraceLocationEnd, float Radius, FName TraceTag, bool bIsSingle)
{
	TArray<FHitResult> Hits;
	
	const FCollisionShape Sphere = FCollisionShape::MakeSphere(Radius);
	FCollisionQueryParams CollisionParams = FCollisionQueryParams(TraceTag, false, this);
	if (!bIsSingle)
	{
		GetWorld()->SweepMultiByChannel(Hits, TraceLocationStart, TraceLocationEnd, FQuat::Identity,
			ECollisionChannel::ECC_Pawn, Sphere, CollisionParams);
	}
	else
	{
		FHitResult FirstHit;
		if (GetWorld()->SweepSingleByChannel(FirstHit, TraceLocationStart, TraceLocationEnd, FQuat::Identity,
			ECollisionChannel::ECC_Pawn, Sphere, CollisionParams))
		{
			Hits.Add(FirstHit);
		}
	}

	return Hits;
}

FLDCycleProperties ALD51Character::GetCurrentWorldCycle(UWorld* World)
{
	if (ALD51GameMode* GameMode = GetLDGameMode(World))
	{
		return GameMode->GetDayNightCycler()->GetCurrentCycle();
	}
	
	return FLDCycleProperties();
}

ALD51GameMode* ALD51Character::GetLDGameMode(UWorld* World)
{
	if (ensureMsgf(World != nullptr, TEXT("Can't perform action without world")))
	{
		return Cast<ALD51GameMode>(World->GetAuthGameMode());
	}

	return nullptr;
}

