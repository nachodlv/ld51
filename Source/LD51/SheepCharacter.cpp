// Fill out your copyright notice in the Description page of Project Settings.


#include "SheepCharacter.h"

#include "SheepAIController.h"

#include "Components/CapsuleComponent.h"
#include "LD51GameMode.h"

#include "GameFramework/PawnMovementComponent.h"

// Sets default values
ASheepCharacter::ASheepCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

void ASheepCharacter::SetHealth(float NewHealth)
{
	if (NewHealth < Health)
	{
		const APlayerController* PlayerController = GetWorld()->GetFirstPlayerController();
		if (const APawn* Pawn = PlayerController ? PlayerController->GetPawn() : nullptr)
		{
			if (Pawn->GetDistanceTo(this) > ImmortalityDistance)
			{
				return;
			}
		}
	}
	
	const float OldHealth = Health;
	Health = NewHealth;
	OnHealthChanged.Broadcast(this, NewHealth, OldHealth);

	if (Health <= 0)
	{
		GetCapsuleComponent()->SetCollisionEnabled(ECollisionEnabled::NoCollision);
		FTimerHandle Handle;
		TWeakObjectPtr<ASheepCharacter> This (this);
		FTimerDelegate Delegate = FTimerDelegate::CreateLambda([This]()
		{
			if (This.IsValid())
			{
				if (AController* Controller = This->GetController())
				{
					Controller->Destroy();
				}
				This->Destroy();
			}
		});
		GetWorldTimerManager().SetTimer(Handle, Delegate, 50.0f, false);
	}
}

void ASheepCharacter::ApplyDamage(AActor * DamageInstigator, const FHitResult & Hit, float Damage)
{
	if (!DamageInstigator)
	{
		return;
	}

	float NewHealth = Health - Damage;

	if (NewHealth > 0.0f)
	{
		FVector KnockbackDirection = (GetActorLocation() + FVector::UpVector * 100.0f ) - DamageInstigator->GetActorLocation();
		KnockbackDirection.Normalize();
		// KnockbackDirection.Z = 0.07f;
		GetMovementComponent()->Velocity = FVector::Zero();

		const FVector Destination = GetActorLocation() + KnockbackDirection * 30.0f;
		FCollisionQueryParams Params;
		Params.AddIgnoredActor(this);
		if (!GetWorld()->LineTraceTestByChannel(GetActorLocation(), Destination, ECollisionChannel::ECC_Visibility,Params))
		{
			TeleportTo(Destination, GetActorRotation());
		}
		// LaunchCharacter(KnockbackDirection * KnockbackForce, true, true);
	}
	else
	{
		if (ALD51GameMode* GameMode = GetLDGameMode(GetWorld()))
		{
			GameMode->AddGameScore(ScoreValues[AIKillScoreTag]);
		}
	}

	ShowDamageFeedback(DamageInstigator, Hit, Damage);
	SetHealth(NewHealth);
}

void ASheepCharacter::MontageNotifyReceived(FName NotifyName, const FBranchingPointNotifyPayload& BranchingPointPayload)
{
	if (NotifyName == TEXT("Attack"))
	{
		if (ASheepAIController* AIController = Cast<ASheepAIController>(GetController()))
		{
			AIController->MakeAttack();
		}
	}
}

// Called when the game starts or when spawned
void ASheepCharacter::BeginPlay()
{
	Super::BeginPlay();

	float NewMaxHealth = MaxHealth;
	if (ALD51GameMode* GameMode = GetLDGameMode(GetWorld()))
	{
		NewMaxHealth += NewMaxHealth * (GameMode->GetNightsSurvived() * MaxHealthMultiplier);
	}
	
	SetHealth(NewMaxHealth);

	for (const USkeletalMeshComponent* SkelMesh : GetMeshes())
	{
		SkelMesh->GetAnimInstance()->OnPlayMontageNotifyBegin.AddDynamic(this, &ASheepCharacter::MontageNotifyReceived);
	}
}

bool ASheepCharacter::IsDead() const
{
	return Health <= 0;
}

bool ASheepCharacter::GetIsSpider() const
{
	if (ASheepAIController* SheepAIController = Cast<ASheepAIController>(GetController()))
	{
		return SheepAIController->IsSpider();
	}
	return false;
}

float ASheepCharacter::GetHealthPercentage() const
{
	return Health / MaxHealth;
}

float ASheepCharacter::GetTargetScore(FName ScoreTag) const
{
	if (ScoreValues.Contains(ScoreTag))
	{
		return ScoreValues[ScoreTag];
	}

	return 0.0f;
}

// Called every frame
void ASheepCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

// Called to bind functionality to input
void ASheepCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	PlayerInputComponent->BindAxis(TEXT("MoveForward"), this, &ASheepCharacter::MoveForward);
}

FVector ASheepCharacter::GetShootLocation() const
{
	return GetMesh()->GetSocketLocation(ShootSocketName);
}

void ASheepCharacter::MoveForward(float AxisValue)
{
	AddMovementInput(GetActorForwardVector() * AxisValue);
}

ALD51GameMode* ASheepCharacter::GetLDGameMode(UWorld* World)
{
	if (ensureMsgf(World != nullptr, TEXT("Can't perform action without world")))
	{
		return Cast<ALD51GameMode>(World->GetAuthGameMode());
	}

	return nullptr;
}
