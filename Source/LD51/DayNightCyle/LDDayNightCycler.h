﻿#pragma once

#include "CoreMinimal.h"
#include "UObject/Object.h"
#include "LDDayNightCycler.generated.h"

class ULDDayNightCycler;

UENUM(BlueprintType)
enum class ELDCycleState : uint8
{
	Transition,
	Day,
	Night
};

USTRUCT(BlueprintType)
struct FLDCycleProperties
{
	GENERATED_BODY()

	FLDCycleProperties(ELDCycleState InCycle = ELDCycleState::Transition, float InDuration = 0.0f)
		: Cycle(InCycle), Duration(InDuration) {}
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	ELDCycleState Cycle;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float Duration = 0.0f;
};

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnCycleChange, ULDDayNightCycler*, Cycler, const FLDCycleProperties&, NewCycle);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_ThreeParams(FOnCycleEnded, ULDDayNightCycler*, Cycler, const FLDCycleProperties&, NewCycle, const FLDCycleProperties&, OldCycle);

UCLASS(Blueprintable, BlueprintType)
class LD51_API ULDDayNightCycler : public UObject
{
	GENERATED_BODY()

public:
	ULDDayNightCycler();
	
	UFUNCTION(BlueprintCallable)
	void StartDay();

	UFUNCTION(BlueprintPure)
	const FLDCycleProperties& GetCurrentCycle() const {return States[CurrentStateIndex]; }

	UFUNCTION(BlueprintPure)
	float GetTimeRemaining() const { return GetWorld()->GetTimerManager().GetTimerRemaining(CycleTimer); }

	UPROPERTY(BlueprintAssignable)
	FOnCycleChange OnCycleChange;

	/** Same delegate but with new paremeters */
	UPROPERTY(BlueprintAssignable)
	FOnCycleEnded OnCycleEnded;

protected:
	virtual void BeginDestroy() override;
	
	void CycleEnded();

private:
	UPROPERTY(EditAnywhere, Category = "LD")
	TArray<FLDCycleProperties> States;

	UPROPERTY(EditAnywhere, Category = "LD")
	float FirstDayTime = 20.0f;
	
	FTimerHandle CycleTimer;

	int32 CurrentStateIndex = 0;

	bool bFirstDay = true;
};
