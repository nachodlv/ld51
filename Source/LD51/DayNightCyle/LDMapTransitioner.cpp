﻿#include "LDMapTransitioner.h"

#include "LDDayNightCycler.h"

#include "Engine/LevelStreaming.h"

#include "Kismet/GameplayStatics.h"

#include "LD51/LD51GameMode.h"

void ULDMapTransitioner::Initialize() const
{
	const ALD51GameMode* GameMode = GetWorld()->GetAuthGameMode<ALD51GameMode>();
	ULDDayNightCycler* Cycler = GameMode->GetDayNightCycler();
	Cycler->OnCycleChange.AddDynamic(this, &ULDMapTransitioner::SwapMaps);

	SetMapsVisibility(false);
}

void ULDMapTransitioner::SwapMaps(ULDDayNightCycler* Cycler, const FLDCycleProperties& NewCycle)
{
	SetMapsVisibility(NewCycle.Cycle == ELDCycleState::Night);
}

void ULDMapTransitioner::SetMapsVisibility(bool bIsNight) const
{
	const FName DayLevelName = DayLevel.ToSoftObjectPath().GetLongPackageFName();
	const FName NightLevelName = NightLevel.ToSoftObjectPath().GetLongPackageFName();
	
	for (ULevelStreaming* StreamingLevel : GetWorld()->GetStreamingLevels())
	{
		if (!StreamingLevel)
		{
			continue;
		}

		if (StreamingLevel->PackageNameToLoad == DayLevelName || StreamingLevel->GetWorldAsset() == DayLevel)
		{
			StreamingLevel->SetShouldBeVisible(!bIsNight);
		}
		else if (StreamingLevel->PackageNameToLoad == NightLevelName || StreamingLevel->GetWorldAsset() == NightLevel)
		{
			StreamingLevel->SetShouldBeVisible(bIsNight);
		}
	}

}
