﻿#pragma once

#include "CoreMinimal.h"
#include "LDDayNightCycler.h"

#include "UObject/Object.h"
#include "LDMapTransitioner.generated.h"

UCLASS(Blueprintable, BlueprintType)
class LD51_API ULDMapTransitioner : public UObject
{
	GENERATED_BODY()

public:
	void SetMapsVisibility(bool bIsNight) const;
	void Initialize() const;

protected:
	UFUNCTION()
	void SwapMaps(ULDDayNightCycler* Cycler, const FLDCycleProperties& NewCycle);

private:
	UPROPERTY(EditAnywhere)
	TSoftObjectPtr<UWorld> DayLevel;

	UPROPERTY(EditAnywhere)
	TSoftObjectPtr<UWorld> NightLevel;

	bool bDayLevelLoaded = true;
	
};
