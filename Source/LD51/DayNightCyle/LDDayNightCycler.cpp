﻿#include "LDDayNightCycler.h"

ULDDayNightCycler::ULDDayNightCycler()
{
	FLDCycleProperties Transition = FLDCycleProperties(ELDCycleState::Transition, 2.0f);
	States = {
		Transition,
		FLDCycleProperties(ELDCycleState::Day, 10.0f),
		Transition,
		FLDCycleProperties(ELDCycleState::Night, 10.0f),
	};
}

void ULDDayNightCycler::StartDay()
{
	ensure(CurrentStateIndex == 0);
	CycleEnded();
}

void ULDDayNightCycler::BeginDestroy()
{
	if (const UWorld* World = GetWorld())
	{
		FTimerManager& TimerManager = World->GetTimerManager();
		TimerManager.ClearTimer(CycleTimer);
	}
	UObject::BeginDestroy();
}

void ULDDayNightCycler::CycleEnded()
{
	int32 PreviousCycle = CurrentStateIndex;
	CurrentStateIndex = (CurrentStateIndex + 1) % States.Num();

	const FLDCycleProperties& CurrentCycle = GetCurrentCycle();
	OnCycleChange.Broadcast(this, CurrentCycle);
	OnCycleEnded.Broadcast(this, CurrentCycle, States[PreviousCycle]);

	float Duration = CurrentCycle.Duration;

	if (bFirstDay)
	{
		Duration = FirstDayTime;
		bFirstDay = false;
	}
	
	FTimerManager& TimerManager = GetWorld()->GetTimerManager();
	const FTimerDelegate Delegate = FTimerDelegate::CreateUObject(this, &ULDDayNightCycler::ULDDayNightCycler::CycleEnded);
	TimerManager.SetTimer(CycleTimer, Delegate, Duration, false);
}

