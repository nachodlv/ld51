﻿#include "LDSheepSpawner.h"

#include "Kismet/GameplayStatics.h"

#include "LD51/SheepCharacter.h"


ALDSheepSpawner::ALDSheepSpawner()
{
	PrimaryActorTick.bCanEverTick = false;
}

int32 ALDSheepSpawner::GetQuantityOfEnemies() const
{
	TArray<AActor*> Actors;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), ActorToSpawn, Actors);
	int32 Quantity = 0;
	for (AActor* Actor : Actors)
	{
		const ASheepCharacter* AI = Cast<ASheepCharacter>(Actor);
		if (AI && AI->GetCurrentHealth() > 0.0f)
		{
			++Quantity;
		}
	}

	return Quantity;
}

