﻿#pragma once

#include "CoreMinimal.h"

#include "Engine/TargetPoint.h"

#include "LD51/DayNightCyle/LDDayNightCycler.h"

#include "LDTargetPointSpawner.generated.h"


USTRUCT()
struct FLDSpawnByTime
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere)
	float Time = 0.0f;

	UPROPERTY(EditAnywhere)
	int32 MaxQuantitySpawned = 0;
};

UCLASS(Blueprintable, BlueprintType)
class LD51_API ALDTargetPointSpawner : public ATargetPoint
{
	GENERATED_BODY()

public:

protected:
	virtual void BeginPlay() override;
	
	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

	void TryToSpawn();

	virtual void SpawnActor();

	virtual int32 GetQuantityOfEnemies() const;

	UPROPERTY(EditAnywhere, Category = "LD")
	TSubclassOf<AActor> ActorToSpawn;

private:
	UPROPERTY(EditAnywhere, Category = "LD")
	float Cooldown = 5.0f;

	UPROPERTY(EditAnywhere, Category = "LD")
	TArray<FLDSpawnByTime> QuantitySpawnedBySurvivedTime;

	UPROPERTY(EditDefaultsOnly, Category = "LD")
	TArray<ELDCycleState> CycleToSpawn;

	UPROPERTY(EditAnywhere)
	float MinRangeToPlayer = 2000.0f;

	FTimerHandle SpawnTimer;
};
