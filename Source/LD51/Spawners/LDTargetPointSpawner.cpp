﻿#include "LDTargetPointSpawner.h"

#include "Kismet/GameplayStatics.h"

#include "LD51/LD51GameMode.h"

void ALDTargetPointSpawner::BeginPlay()
{
	Super::BeginPlay();
	FTimerManager& TimerManager = GetWorld()->GetTimerManager();
	const FTimerDelegate Delegate = FTimerDelegate::CreateUObject(this, &ALDTargetPointSpawner::TryToSpawn);
	TimerManager.SetTimer(SpawnTimer, Delegate, Cooldown, true);
}

void ALDTargetPointSpawner::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	if (const UWorld* World = GetWorld())
	{
		FTimerManager& TimerManager = World->GetTimerManager();
		TimerManager.ClearTimer(SpawnTimer);
	}
	
	Super::EndPlay(EndPlayReason);
}

void ALDTargetPointSpawner::TryToSpawn()
{
	const ALD51GameMode* GameMode = GetWorld()->GetAuthGameMode<ALD51GameMode>();
	const ULDDayNightCycler* Cycler = GameMode->GetDayNightCycler();
	const FLDCycleProperties& CycleProperties = Cycler->GetCurrentCycle();

	if (const APlayerController* FirstPlayerController = GetWorld()->GetFirstPlayerController())
	{
		const APawn* Player = FirstPlayerController->GetPawn();
		if (GetDistanceTo(Player) < MinRangeToPlayer)
		{
			return;
		}
	}
	
	if (CycleToSpawn.Contains(CycleProperties.Cycle))
	{
		const float Playtime = GameMode->GetPlaytime();
		const int32 QuantityOfEnemies = GetQuantityOfEnemies();

		bool bLimitReached = !QuantitySpawnedBySurvivedTime.Num() == 0;

		if (bLimitReached)
		{
			for (int32 i = 0; i < QuantitySpawnedBySurvivedTime.Num(); ++i)
			{
				const FLDSpawnByTime& SpawnByTime = QuantitySpawnedBySurvivedTime[i];
				if (SpawnByTime.Time > Playtime)
				{
					if (i > 0 && QuantityOfEnemies < QuantitySpawnedBySurvivedTime[i-1].MaxQuantitySpawned)
					{
						bLimitReached = false;
						break;
					}
				}
			}
			if (bLimitReached)
			{
				bLimitReached = QuantitySpawnedBySurvivedTime.Last().Time > Playtime || QuantitySpawnedBySurvivedTime.Last().MaxQuantitySpawned < QuantityOfEnemies;
			}
		}

		if (!bLimitReached)
		{
			const FVector StartTrace = GetActorLocation() + FVector::UpVector * 100.0f;
			const FVector EndTrace = StartTrace - FVector::UpVector * 500.0f;
			const FCollisionObjectQueryParams Params (ECollisionChannel::ECC_WorldDynamic);
			const FCollisionShape Sphere = FCollisionShape::MakeSphere(50.0f);
			TArray<FHitResult> Hits;
			GetWorld()->SweepMultiByObjectType(Hits, StartTrace, EndTrace, FQuat::Identity, Params, Sphere);
			bool bAIHit = false;
			for (const FHitResult& Hit : Hits)
			{
				bAIHit = Hit.GetActor()->IsA(ActorToSpawn);
			}
			if (!bAIHit)
			{
				SpawnActor();
			}
		}
	}
}

void ALDTargetPointSpawner::SpawnActor()
{
	GetWorld()->SpawnActor<AActor>(ActorToSpawn, GetActorLocation(), GetActorRotation());
}

int32 ALDTargetPointSpawner::GetQuantityOfEnemies() const
{
	TArray<AActor*> Actors;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), ActorToSpawn, Actors);
	return Actors.Num();
}
