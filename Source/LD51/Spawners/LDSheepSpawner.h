﻿#pragma once

#include "CoreMinimal.h"
#include "LDTargetPointSpawner.h"

#include "GameFramework/Actor.h"
#include "LDSheepSpawner.generated.h"

UCLASS()
class LD51_API ALDSheepSpawner : public ALDTargetPointSpawner
{
	GENERATED_BODY()

public:
	ALDSheepSpawner();

protected:
	virtual int32 GetQuantityOfEnemies() const override;
};
