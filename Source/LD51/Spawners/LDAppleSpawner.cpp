﻿
#include "LDAppleSpawner.h"

#include "LD51/Apple.h"


ALDAppleSpawner::ALDAppleSpawner()
{
	PrimaryActorTick.bCanEverTick = false;
}

void ALDAppleSpawner::BeginPlay()
{
	Super::BeginPlay();
}

void ALDAppleSpawner::SpawnActor()
{
	const FTransform Transform (GetActorRotation(), GetActorLocation());
	AApple* NewApple = GetWorld()->SpawnActorDeferred<AApple>(ActorToSpawn, Transform);
	NewApple->SetBuried(FMath::RandRange(0.0f, 1.0f) < ChanceToSpawnBuried);
	NewApple->FinishSpawning(Transform);
}

