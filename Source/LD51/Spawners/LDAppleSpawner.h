﻿#pragma once

#include "CoreMinimal.h"
#include "LDTargetPointSpawner.h"

#include "GameFramework/Actor.h"
#include "LDAppleSpawner.generated.h"

UCLASS()
class LD51_API ALDAppleSpawner : public ALDTargetPointSpawner
{
	GENERATED_BODY()

public:
	ALDAppleSpawner();

protected:
	virtual void BeginPlay() override;

	virtual void SpawnActor() override;

private:
	UPROPERTY(EditAnywhere)
	float ChanceToSpawnBuried = 0.5f;
};
