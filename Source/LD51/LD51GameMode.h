// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"

#include "DayNightCyle/LDDayNightCycler.h"

#include "GameFramework/GameModeBase.h"
#include "LD51GameMode.generated.h"

class ULDMapTransitioner;
class ULDDayNightCycler;

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnGameScoreUpdated, ALD51Character*, Character);

UCLASS(minimalapi)
class ALD51GameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ALD51GameMode();

	virtual void InitGame(const FString& MapName, const FString& Options, FString& ErrorMessage) override;
	
	virtual void StartPlay() override;

	UFUNCTION(BlueprintPure)
	ULDDayNightCycler* GetDayNightCycler() const { return DayNightCycler; }

	UFUNCTION(BlueprintPure)
	ULDMapTransitioner* GetMapTransitioner() const { return MapTransitioner; }

	UFUNCTION(BlueprintPure)
	float GetPlaytime() const { return GetWorld()->GetTimeSeconds() - GameplayStartTime; }

	UFUNCTION(BlueprintPure)
	float GetGameScore() const { return GameScore; }

	UFUNCTION(BlueprintCallable)
	void AddGameScore(float InScoreValue);

	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent)
	void GameOver();

	UFUNCTION(BlueprintPure)
	int32 GetNightsSurvived() const { return NightsSurvived; }

	UPROPERTY(BlueprintAssignable)
	FOnGameScoreUpdated OnGameScoreUpdated;

protected:
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void OnCycleChange(ULDDayNightCycler* Cycler, const FLDCycleProperties& NewCycle, const FLDCycleProperties& PreviousCycle);
	void OnCycleChange_Implementation(ULDDayNightCycler* Cycler, const FLDCycleProperties& NewCycle, const FLDCycleProperties& PreviousCycle);
	
private:
	UPROPERTY(EditAnywhere)
	TSubclassOf<ULDDayNightCycler> DayNightCyclerClass;

	UPROPERTY(EditAnywhere)
	TSubclassOf<ULDMapTransitioner> MapTransitionerClass;

	UPROPERTY(Transient)
	TObjectPtr<ULDDayNightCycler> DayNightCycler = nullptr;
	
	UPROPERTY(Transient)
	TObjectPtr<ULDMapTransitioner> MapTransitioner = nullptr;

	UPROPERTY(EditAnywhere, Category = "LD")
	float ScoreForNight = 50.0f;

	float GameplayStartTime = 0.0f;

	float GameScore = 0.0f;

	int32 NightsSurvived = 0;
};



